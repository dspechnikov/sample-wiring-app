# Sample wiring automation app

[![pipeline status](https://gitlab.com/dspechnikov/sample-wiring-app/badges/master/pipeline.svg)](https://gitlab.com/dspechnikov/sample-wiring-app/commits/master)
[![coverage report](https://gitlab.com/dspechnikov/sample-wiring-app/badges/master/coverage.svg)](https://gitlab.com/dspechnikov/sample-wiring-app/commits/master)

## Problem

There are `n` clients which need to be distributed between `k`
power hubs so the sum of power cables is minimal. Each power hub can
support maximum of `p` clients.

## Assumptions

For this sample app, the following assumptions are used:

- Each hub is almost fully filled with client so there is no "free" hubs to
further optimize wiring configuration.
- There are enough hubs to connect every client.

## Approach

This problem can be interpreted as search for `k` samples of points
from `n` so their geometric medians give the least possible
sum of distances to corresponding sample points.

The general idea is to split given points to non-intersecting "islands",
thus avoiding to count intersections. "Brute force" solution would be to
choose a point from convex hull of all points, calculate geometric
median for it and all combinations of `p - 1` remaining points and then
choose the minimum.

However, it's extremely slow because it has factorial complexity.
In reality, there may be hundreds of clients, so it's impossible to compute the
result in reasonable time.

Another approach (which is implemented here) is to take `p - 1` nearest points
to the point from convex hull. It can be slightly suboptimal in terms of total
sum of distances, but is dramatically faster.

## Usage

App takes input data in form of CSV file with comma as a separator.
Input file should not contain header row.
Each data row should be in the following format:

`client_id,x_coordinate,y_coordinate,z_coordinate`

- client_id is a string.
- coordinates may be integers or floats.

#### References

- Geometric median is calculated using improved Weiszfeld's algorithm
described by Yehuda&nbsp;Vardi and Cun&#8209;Hui&nbsp;Zhang in their
[paper](http://www.pnas.org/content/97/4/1423.full.pdf).
