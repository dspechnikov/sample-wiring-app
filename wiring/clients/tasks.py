from operator import itemgetter

import numpy as np
from scipy.spatial.distance import euclidean

from wiring.clients.sample import find_closest_points, gmedian


def prepare_clients_data(data):
    clients = [(cid, float(x), float(y), float(z))
               for cid, x, y, z in data]

    return sorted(clients, key=itemgetter(1, 2, 3))


def distribute_clients(clients, hubs_count, clients_per_hub):
    clients = prepare_clients_data(clients)
    points = np.array([client[1:4] for client in clients])

    hubs_info = []
    while points.shape[0] > 0 and len(hubs_info) < hubs_count:
        closest_clients = find_closest_points(
            points[0],
            np.delete(points, 0, 0),
            count=clients_per_hub - 1
        )
        sample_clients = np.concatenate(([points[0]], closest_clients))

        hub_position = gmedian(sample_clients)
        hubs_info.append({
            'position': hub_position,
            'clients': [{
                'id': next(cid for cid, x, y, z in clients
                           if [x, y, z] == list(client)),
                'cable_len': euclidean(client, hub_position)
            } for client in sample_clients]
        })

        # remove distributed clients from points list
        for point in sample_clients:
            points = np.array([p for p in points if not (p == point).all()])

    return hubs_info
