import numpy as np
import pytest
from scipy.spatial.distance import euclidean

from wiring.clients.sample import gmedian


class TestGMedian:
    @pytest.mark.parametrize('label,points,expected', [
        ('multiple dimensions',
         np.array([[0, 0, 0], [0, 1, 0], [0, 0, 1], [1, 0, 1]]),
         np.array([0.183, 0.183, 0.5])),
        ('exact median',
         np.array([[0, 0], [0, 1], [0, 0.7]]),
         np.array([0, 0.7])),
    ])
    def test_median_value_coords(self, label, points, expected):
        assert euclidean(gmedian(points), expected) < 1e-4

    def test_non_unique_median(self):
        points = np.array([[0, 0], [0, 1]])

        median = gmedian(points)

        assert median[0] == 0
        assert 0 < median[1] < 1

    def test_custom_precision(self):
        points = np.array([[0, 1], [1, 1], [1, 0], [0, 0], [2, 0.7], [3, 0.5]])

        median = gmedian(points, eps=1e-5)

        assert euclidean(median, np.array([1.04608601, 0.56948672])) < 1e-5
