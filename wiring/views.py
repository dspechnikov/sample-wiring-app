import csv

import aiohttp_jinja2

from wiring.clients.tasks import distribute_clients
from wiring.clients.validators import ValidationError, validate_clients


def get_context_data(hubs_info):
    hubs = {}

    for hub_id, hub_info in enumerate(hubs_info, start=1):
        hubs[hub_id] = {
            'position': [round(c, 3) for c in hub_info['position']],
            'pins': (
                (pin_id, client_info['id'], round(client_info['cable_len'], 3))
                for pin_id, client_info
                in enumerate(hub_info['clients'], start=1)
            )
        }

    return {
        'hubs': hubs
    }


@aiohttp_jinja2.template('index.html')
async def index(request):
    if request.method == 'GET':
        return {}

    data = await request.post()
    clients_data = list(csv.reader(
        r.decode() for r in data['input_data'].file
    ))

    try:
        validate_clients(clients_data)
    except ValidationError as e:
        return {
            'errors': [str(e)]
        }

    return get_context_data(
        distribute_clients(
            clients_data,
            hubs_count=6,
            clients_per_hub=10
        )
    )
