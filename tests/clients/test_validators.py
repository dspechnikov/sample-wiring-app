import pytest

from wiring.clients.validators import validate_clients, ValidationError


def test_valid_data():
    clients_data = (
        ['a', '1', '2', '3'],
        ['b', '0', '0', '0'],
    )

    assert validate_clients(clients_data) is None


@pytest.mark.parametrize('label,data', [
    ('csv-with-header',
     (
        ['id', 'x', 'y', 'z'],
        ['a', '1', '2', '3'],
        ['b', '0', '0', '0'],
     )),

    ('non-float-coordinates',
     (
         ['a', 'x', '2', '3'],
         ['a', 'x', '2', '3'],
     )),

    ('missing-fields',
     (
         ['a', '1', '2', None],
         ['a', '1', '2', None],
     )),

    ('only-one-client',
     (
             ['a', '1', '2', '3'],
     )),
])
def test_invalid_data(label, data):
    with pytest.raises(ValidationError):
        validate_clients(data)
