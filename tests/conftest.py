import aiohttp_jinja2
import jinja2
import pytest
from aiohttp import web

from wiring.routes import setup_routes


@pytest.fixture()
def client(loop, test_client):
    app = web.Application(loop=loop)
    setup_routes(app)
    aiohttp_jinja2.setup(
        app,
        loader=jinja2.PackageLoader('wiring', 'templates')
    )

    return loop.run_until_complete(test_client(app))
