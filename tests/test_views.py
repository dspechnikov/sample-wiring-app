from io import StringIO


async def test_get(client):
    response = await client.get('/')

    assert response.status == 200


async def test_upload_csv_with_errors(client):
    clients_data = StringIO('a,1,2,3')

    response = await client.post('/', data={'input_data': clients_data})
    assert response.status == 200

    body = await response.text()
    assert 'must be at least 2 clients' in body


async def test_distribution_results(client):
    clients_data = StringIO('a,1,2,3\nb,0,0,0')

    response = await client.post('/', data={'input_data': clients_data})
    assert response.status == 200

    body = await response.text()
    assert 'Results' in body
