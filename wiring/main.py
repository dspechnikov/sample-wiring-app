import os

import aiohttp_jinja2
import jinja2
from aiohttp import web

from wiring.routes import setup_routes


app = web.Application()

setup_routes(app)
aiohttp_jinja2.setup(
    app,
    loader=jinja2.PackageLoader('wiring', 'templates')
)

web.run_app(app, host='0.0.0.0', port=int(os.environ.get('PORT', 5000)))
