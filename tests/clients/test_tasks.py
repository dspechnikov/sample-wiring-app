from math import sqrt

import pytest

from wiring.clients.tasks import distribute_clients


@pytest.mark.parametrize('clients,hubs,clients_per_hub,expected', [
    ([
        ["1", 0, 0, 0],
        ["2", 0, 0, 1],
        ["3", 0, 1, 0],
        ["4", 0, 1, 1],
        ["5", 1, 0, 0],
        ["6", 1, 1, 0],
        ["7", 1, 0, 1],
        ["8", 1, 1, 1],
     ], 1, 8, 4*sqrt(3)),
    ([
        ["1", 0, 0, 0],
        ["2", 0, 0, 1],
        ["3", 0, 1, 0],
        ["4", 0, 1, 1],
        ["5", 1, 0, 0],
        ["6", 1, 1, 0],
        ["7", 1, 0, 1],
        ["8", 1, 1, 1],
     ], 2, 4, 5.7735),
])
def test_clients_distribution(clients, hubs, clients_per_hub, expected):
    hubs = distribute_clients(clients, hubs, clients_per_hub)

    sum_len = sum(client['cable_len']
                  for hub_info in hubs
                  for client in hub_info['clients'])

    assert abs(sum_len - expected) < 1e-4
