class ValidationError(RuntimeError):
    pass


def validate_clients(data):
    if len(data) < 2:
        raise ValidationError(
            'There must be at least 2 clients to distribute.'
        )

    for i, row in enumerate(data, start=1):
        try:
            [float(row[coord]) for coord in range(1, 4)]
        except (ValueError, TypeError):
            raise ValidationError('Row #%d has invalid coordinates.' % i)
