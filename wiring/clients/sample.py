import numpy as np
from scipy.spatial.distance import cdist, euclidean


def gmedian(points, eps=1e-4):
    """
    Computes geometric median of given points with given precision.

    Algorithm is described in this paper:
    http://www.pnas.org/content/97/4/1423.full.pdf
    """
    y = np.mean(points, 0)

    while True:
        distances = cdist(points, [y])
        nonzeros = (distances != 0)[:, 0]

        Dinv = 1 / distances[nonzeros]
        Dinv_sum = np.sum(Dinv)
        T = np.sum((Dinv / Dinv_sum) * points[nonzeros], 0)

        num_zeros = len(points) - np.sum(nonzeros)
        if num_zeros == len(points):
            return y
        elif num_zeros == 0:
            y1 = T
        else:
            R = (T - y) * Dinv_sum
            r = np.linalg.norm(R)
            rinv = 0 if r == 0 else num_zeros / r

            y1 = max(0, 1 - rinv) * T + min(1, rinv) * y

        if euclidean(y, y1) < eps:
            return y1

        y = y1


def find_closest_points(point, points, count=1):
    """Given a point, returns `count` closest points from points list."""
    if not points.size:
        raise ValueError('`points` must be a 2-dimensional array with '
                         'at least one point')

    closest_points = np.empty((0, 3))

    while points.size > 0 and closest_points.shape[0] < count:
        closest_idx = cdist([point], points).argmin()

        closest_points = np.append(closest_points, [points[closest_idx]], 0)
        points = np.delete(points, closest_idx, 0)

    return closest_points
